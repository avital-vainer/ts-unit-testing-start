

export function sum(...nums: number[]): number {
    return nums.reduce((total, num) => total + num);
}


export function multiply(mult: number, ...nums: number[]): number[] {
    return nums.map((num) => mult * num);
}