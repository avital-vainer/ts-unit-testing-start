import { expect } from "chai";
import { multiply, sum } from "../src/calc";

describe("The calc module", () => {

    context("#sum", () => {
        it("should exist", () => {
            expect(sum).to.be.a("function");
            expect(sum).to.be.instanceOf(Function);
        });

        it("should sum two numbers", () => {
            const actualSum = sum(4, 6);
            expect(actualSum).to.equal(10);
        });

        it("should sum several numbers", () => {
            const actualSum = sum(1, 2, 3, 4, 5);
            expect(actualSum).to.equal(15);
        });
    });


    context("#multiply", () => {
        it("should exist", () => {
            expect(multiply).to.be.a("function");
            expect(multiply).to.be.instanceOf(Function);
        });

        it("should multiply two numbers", () => {
            const mult = multiply(4, 6);
            expect(mult).to.eql([24]);
        });

        it("should multiply several numbers", () => {
            const mult = multiply(2, 2, 4, 6, 8);
            expect(mult).to.eql([4, 8, 12, 16]);
        });
    });


    context("#async tests", () => {

        const delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

        it("should multiply several numbers with delay", async () => {
            await delay(500);
            const mult = multiply(2, 2, 4, 6, 8);
            expect(mult).to.eql([4, 8, 12, 16]);
        });
    });
});